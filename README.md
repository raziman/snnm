# Snnm
Scrape synonym words from [thesaurus.com](https://www.thesaurus.com)

## Installing

Clone the repo and cd into it
```sh
$ git clone https://gitlab.com/raziman/snnm
$ cd snnm
```

Install
```sh
$ npm install -g .
```
