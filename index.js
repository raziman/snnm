#!/usr/bin/env node
const cheerio = require("cheerio")
const { exec } = require("child_process")

if (process.argv.length < 3) {
	console.log("usage: syn <word>")
	return
}

const word = process.argv[2]


exec(`curl https://www.thesaurus.com/browse/${word}`, (err, stdout, stderr) => {
	if (err) {
		console.error(err, stderr)
		return
	}

	const $ = cheerio.load(stdout)
	const meanings = $('#meanings')
	const list = $('li', meanings)

	list.each((_, el) => {
		const data = $(el).text()
		console.log(data)
	})
})

